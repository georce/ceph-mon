FROM distribution.alauda.cn/georce/ceph-base

RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ADD entrypoint.sh /entrypoint.sh

VOLUME ["/etc/ceph","/var/lib/ceph"]

EXPOSE 6789

ENTRYPOINT ["/entrypoint.sh"]
